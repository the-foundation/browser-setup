## merged from https://gist.github.com/powerman/b90f3a63fa2f3583d3e89b2468f95388
## merged from https://media.kuketz.de/blog/artikel/2018/about:config/user.js
## merged from https://www.maketecheasier.com/28-coolest-firefox-aboutconfig-tricks/

##QUICK INSTALL: (bash linux)
## [[ -z   "$profile" ]] && { cd ~/.mozilla/firefox/;echo listing profiles..pick a path ;echo; cat profiles.ini |grep -e Name -e Path -e '\[Profile'|sed 's/$/ /g'|tr -d '\n'|sed 's/\[Profile/\nProfile/g'; echo;echo -n "enter profile path: " ;read profile ;} ;test -e "$profile/user-overrides.js" || wget -O- https://the-foundation.gitlab.io/browser-setup/user-overrides.js > "$profile/user-overrides.js" ; bash updater.sh -s -p "$profile";profile=""

## for use with https://raw.githubusercontent.com/ghacksuserjs/ghacks-user.js/master/updater.sh

### custom

# set cache size kb
user_pref("browser.cache.disk.capacity", 128000);
## quick delay for addon install
user_pref("security.dialog_enable_delay", 123);

## Adjust the Smart Location Bar’s Number of Suggestions -- disable
user_pref("browser.urlbar.maxRichResults", -1);

###


#############
## PRIVACY ##
#############

## Disable Domain Guessing
user_pref("browser.fixup.alternate.enabled", false);

## Disable Normandy/Shield (FF60+)
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.api_url", "");
user_pref("app.shield.optoutstudies.enabled", false);

## Disable "Savant" Shield study (FF61+) 
user_pref("shield.savant.enabled", false);

## Disable "Snippets" (Mozilla content shown on about:home screen)
user_pref("browser.aboutHomeSnippets.updateUrl", "");

## Disable Activity Stream (AS)
# Disable AS Telemetry
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry.ping.endpoint", "");
# Disable AS Snippets
user_pref("browser.newtabpage.activity-stream.disableSnippets", true);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
## Disable AS in new Tabs
user_pref("browser.library.activity-stream.enabled", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.enabled", false);
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtab.preload ", false);

## Disable PingCentre Telemetry
user_pref("browser.ping-centre.telemetry", false);
user_pref("browser.ping-centre.production.endpoint", "");
user_pref("browser.ping-centre.staging.endpoint", "");

## Disable Onboarding (FF55+) | Onboarding uses Google Analytics and leaks resource://URIs
user_pref("browser.onboarding.enabled", false);

## Disable Safe Browsing
# Disable binaries NOT in local lists being checked by Google (real-time checking)
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
# Disable "Block dangerous downloads" (under Options>Privacy & Security)
user_pref("browser.safebrowsing.downloads.enabled", false);
# This covers deceptive sites such as phishing and social engineering
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
# Disable "Warn me about unwanted and uncommon software" (under Options>Privacy & Security)
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
# Disable Mozilla's blocklist for known Flash tracking/fingerprinting
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
# Disable reporting URLs
user_pref("browser.safebrowsing.provider.google.reportURL", "");
user_pref("browser.safebrowsing.reportPhishURL", "");
user_pref("browser.safebrowsing.provider.google4.reportURL", ""); 
user_pref("browser.safebrowsing.provider.google.reportMalwareMistakeURL", ""); 
user_pref("browser.safebrowsing.provider.google.reportPhishMistakeURL", ""); 
user_pref("browser.safebrowsing.provider.google4.reportMalwareMistakeURL", ""); 
user_pref("browser.safebrowsing.provider.google4.reportPhishMistakeURL", "");
# Disable data sharing (FF58+)
user_pref("browser.safebrowsing.provider.google4.dataSharing.enabled", false);
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");

## Disable location bar LIVE search suggestions
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.suggest.searches", false);

## Disable Slow Startup Notifications and Telemetry
user_pref("browser.slowStartup.notificationDisabled", true);
user_pref("browser.slowStartup.maxSamples", 0);
user_pref("browser.slowStartup.samples", 0);

## Disable sending of crash reports (FF44+)
user_pref("browser.tabs.crashReporting.sendReport", false);
user_pref("browser.crashReports.unsubmittedCheck.enabled", false);
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false);

## Disable Health Report
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);

## Disable Extension Metadata updating to addons.mozilla.org
user_pref("extensions.getAddons.cache.enabled", false);

## Disable Mozilla permission to silently opt you into tests 
user_pref("network.allow-experiments", false);

## Disable Telemetry
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false); 
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false); 
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false); // 
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false); 
user_pref("toolkit.telemetry.hybridContent.enabled", false);

## Disable experiments
user_pref("experiments.enabled", false);
user_pref("experiments.manifest.uri", "");
user_pref("experiments.supported", false);
user_pref("experiments.activeExperiment", false);

## Disable uploading to the Screenshots server
user_pref("extensions.screenshots.upload-disabled", true);

##############
## SECURITY ##
##############

## Enforce Punycode for Internationalized Domain Names to eliminate possible spoofing
user_pref("network.IDN_show_punycode", true);

## Display all parts of the URL in the location bar eg. http(s):// 
user_pref("browser.urlbar.trimURLs", false);

## Display "insecure" icon (FF59+) and "Not Secure" text (FF60+) on HTTP sites
user_pref("security.insecure_connection_icon.enabled", true);
user_pref("security.insecure_connection_icon.pbmode.enabled", true);
user_pref("security.insecure_connection_text.enabled", true);
user_pref("security.insecure_connection_text.pbmode.enabled", true);


/* [DEV] Avoid /etc/hosts & local DNS breakage when using SOCKS proxy. */
user_pref("network.proxy.socks_remote_dns", false);                     // 0704

/* [UX] Wanna search DuckDuckGo from location bar. */
user_pref("keyword.enabled", true);                                     // 0801

/* [UX,-HIST] Enable search and form history. */
user_pref("browser.formfill.enable", true);                             // 0860

/* [SECURITY] Disable saving passwords. */
user_pref("signon.rememberSignons", false);                             // 0901

/* [PERF,-FP] Use cache.
 * Use ETag Stoppa extension to block at least ETag. */
user_pref("browser.cache.disk.enable", true);                           // 1001

/* [UX,-HIST] Remember more closed tabs for undo. */
user_pref("browser.sessionstore.max_tabs_undo", 42);                    // 1020

/* [UX,-HIST] Restore all state for closed tab or previous session after Firefox restart. */
//user_pref("browser.sessionstore.privacy_level", 0);                     // 1021
user_pref("browser.sessionstore.interval", 150000);                      // 1023

/* [UX] Enable favicons in web notifications. */
user_pref("alerts.showFavicons", true);                                 // 1032

/* [PERF,-FP] Use SSL sessions. */
user_pref("security.ssl.disable_session_identifiers", false);           // 1204
 // 1212

/* [UX] Enable insecure passive content (such as images) on https pages.
 * Use uMatrix to disable it globally and then enable on per-site basis. */
user_pref("security.mixed_content.block_display_content", false);       // 1241

/* [UX] Speedup detection of HTTP-only websites. */
user_pref("dom.security.https_only_mode_send_http_background_request", true); // 1246

/* [SECURITY,-FP] Disable weak ciphers. */
user_pref("security.ssl3.rsa_des_ede3_sha", false);                     // 1261
user_pref("security.ssl3.ecdhe_ecdsa_aes_256_sha", false);              // 1264
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", false);              // 1264
user_pref("security.ssl3.ecdhe_rsa_aes_128_sha", false);                // 1264
user_pref("security.ssl3.ecdhe_rsa_aes_256_sha", false);                // 1264
user_pref("security.ssl3.rsa_aes_128_sha", false);                      // 1264
user_pref("security.ssl3.rsa_aes_256_sha", false);                      // 1264

/* [-TRACKING] Always send CROSS ORIGIN referer.
 * Required for Atlassian JIRA cloud. */
user_pref("network.http.referer.XOriginPolicy", 0);                     // 1603
