#!/bin/bash
#exturl="https://addons.mozilla.org/de/firefox/addon/buttercup-pw/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search" ;
favicon="https://gitlab.com/favicon.ico"
for file in *.list;do
    outname=${file//.list/.snip}
    cat $file |grep ^http |while read exturl;do
        rawinput=$(curl -L "$exturl")
        link=$(echo "$rawinput"|sed 's/http/\nhttp/g'|grep xpi|grep ^https://|cut -d\" -f1 |grep xpi$ );
        icon=$(echo "$rawinput"|sed 's/http/\nhttp/g'| grep "/user-media/addon_icons/"|grep ^https://|cut -d\" -f1|cut -d"?" -f1);
        name=$(echo "$rawinput"|sed 's/class=/\nclass=/g;'| grep 'class="AddonTitle"'|cut -d"<" -f1|cut -d">" -f2 |sed 's/ Kostenloser / /g;s/ Free / /g')
        echo "name $name $link $link"
        echo '<br><div class="link"><a target="_blank" href="'$link'"> Get '$name' <img src="'$icon'" ></a> | <a href="'$link'"> ( Install from this page <img src="'$favicon'" > )</a></div>
              ' >> ${outname}
    done &
done
wait

mkdir .public
cp -r * .public
rm .public/*.sh
mv .public public

ls public
